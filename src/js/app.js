const HOST_URL  = 'ws://localhost:3001';


const MainApp = new class {

    ////////////////////////////////////////////////////////////////////////////
    // public methods

    initialize() {
        this._showStatus('INITIALIZING');

        // Create objects
        this._socket = new Socket(HOST_URL);

        // Register event listeners
        Recorder.onData = this._onRecordingData.bind(this);
        this._socket.onStreamData = this._onStreamData.bind(this);
        this._socket.onEvent = this._onEvent.bind(this);

        // Initialize audio recorder and player
        const audioCtx = new AudioContext();

        return Promise.all([
            Recorder.initialize(audioCtx),
            Player.initialize(audioCtx)
        ]).then(() => {
            this._showStatus('IDLE');
        }).catch((err) => {
            alert(err.message || err.name);
            throw err;
        });
    }

    toggleRecording(element) {
        if (Recorder.isRecording) {
            this._socket.sendCommand('stop');
            Recorder.stop();
            this._showStatus('IDLE');
        } else {
            this._socket.sendCommand('start');
            Recorder.start();
            this._showStatus('RECORDING');
            this._showTranscription('[EMPTY]');
        }
    }


    ////////////////////////////////////////////////////////////////////////////
    // private methods

    _showStatus(text) {
        document.getElementById('status').innerHTML = text;
    }

    _showTranscription(text) {
        document.getElementById('transcription').innerHTML = text;
    }

    _addRecordedClip(fileId) {
        const table = document.getElementById('clips');
        const row = table.insertRow(-1);
        const cell1 = row.insertCell(-1);
        const cell2 = row.insertCell(-1);
        const cell3 = row.insertCell(-1);

        const img = document.createElement('img');
        img.src = '../images/play_button.png';
        img.width = img.height = 20;
        img.addEventListener('click', () => {
            this._socket.sendCommand('reqfile', fileId);
        });

        cell1.innerHTML = table.rows.length;
        cell2.innerHTML = document.getElementById('transcription').innerHTML;
        cell3.appendChild(img);
    }

    // Event listeners

    _onRecordingData(data) {
        this._socket.sendStream(data);
    }

    _onStreamData(data) {
        Player.start(data);
    }

    _onEvent(data) {
        try {
            console.log(data);
            const command = JSON.parse(data);

            switch (command.type.toLowerCase()) {
                case 'bos':
                    console.log('Begin-of-Speech detected.');
                    break;
                case 'eos':
                    console.log('End-of-Speech detected.');
                    this._socket.sendCommand('stop');
                    Recorder.stop();
                    this._showStatus('IDLE');
                    break;
                case 'done':
                    const fileId = command.args[0];
                    this._addRecordedClip(fileId);
                    break;
                case 'transcription':
                    const transcription = command.args[0]
                    console.log(`transcription=${transcription}`);
                    this._showTranscription(transcription);
                    break;
                case 'error':
                    const message = command.args[0] || 'error';
                    console.error(message);
                    alert(message);
                    break;
                default:
                    console.error(`Invalid command type (${command.type})`);
                    break;
            }
        } catch (err) {
            console.error(`Invalid command.`);
        }
    }
}


const initialize = () => { MainApp.initialize(); };
const toggleRecording = (element) => { MainApp.toggleRecording(element); };


window.addEventListener('load', initialize);

