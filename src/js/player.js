/**
 * class AudioPlayer
 */
const Player = new class AudioPlayer {

    ////////////////////////////////////////////////////////////////////////////
    // public methods

    constructor() {
        this._audioCtx = null;
        this._audioSrcPlaying = null;

        this._onEnd = null;
    }

    initialize(audioCtx) {
        this._audioCtx = audioCtx;
        return Promise.resolve();
    }

    // Getters & Setters
    get isPlaying() { return (this._audioSrcPlaying !== null); }
    set onEnd(callback) {
        if (typeof callback !== 'function') throw new Error('Callback must be a function.');
        this._onEnd = callback;
    }

    start(stream) {
        this.stop();

        this._audioCtx.decodeAudioData(stream)
        .then((buffer) => {
            const audioSrc = this._audioCtx.createBufferSource();
            audioSrc.buffer = buffer;
            audioSrc.onended = (event) => { if (this._onEnd) this._onEnd(); };

            audioSrc.connect(this._audioCtx.destination);
            audioSrc.start();
            this._audioSrcPlaying = audioSrc;
        })
        .catch(err => { alert(err); });
    }

    stop() {
        if (this._audioSrcPlaying) {
            this._audioSrcPlaying.stop();
            this._audioSrcPlaying.disconnect();
            this._audioSrcPlaying = null;
        }
    }
}

