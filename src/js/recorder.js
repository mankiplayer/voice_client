const IN_SAMPLE_RATE    = 44100;    // Number of input samples per second (Hertz)
const OUT_SAMPLE_RATE   = 16000;    // Number of output samples per second (Hertz)
const BYTES_PER_SAMPLE  = 2         // 16-Bit(2-Byte) per sample


/**
 * class Recorder
 */
const Recorder = new class AudioRecorder {

    ////////////////////////////////////////////////////////////////////////////
    // public methods

    constructor() {
        // Audio interfaces
        this._audioSource = null;
        this._processNode = null;

        // Variables
        this._isRecording = false;

        // Callback
        this._onData = null;
    }

    initialize(audioCtx) {
        if (!AudioRecorder._checkAudioRecorderAvailable())
            throw new Error('This device does not support capturing audio.');

        return navigator.mediaDevices.getUserMedia({audio: true})
        .then((stream) => {
            // Setup audio source and process node
            this._audioSource = audioCtx.createMediaStreamSource(stream);
            this._processNode = audioCtx.createScriptProcessor({numberOfInputChannels: 1});

			// Register audio process callback
            this._processNode.onaudioprocess = (event) => {
                if (this._isRecording) this._processAudio(event.inputBuffer);
            };
            // WARN! It won't work without this line of code (maybe a bug in Chrome?)
            this._processNode.connect(audioCtx.destination);
        })
        .catch((err) => throw err);
    }

    // Getters & Setters
    get isRecording() { return this._isRecording; }
    set onData(callback) {
        if (typeof callback !== 'function') throw new Error('callback must be a function.');
        this._onData = callback;
    }

    start() {
        // Connect
        this._audioSource.connect(this._processNode);

        this._isRecording = true;
        console.log('Recording started.');
    }

    stop() {
        // Disconnect
        this._audioSource.disconnect();

        this._isRecording = false;
        console.log('Recording stopped.');
    }


    ////////////////////////////////////////////////////////////////////////////
    // private methods

    static _checkAudioRecorderAvailable() {
        if (!navigator || !navigator.mediaDevices) return false;
        if (!navigator.mediaDevices.getUserMedia) return false;
        return true;
    }

    static _resampleTo16000(inputBuffer) {
        const offlineCtx = new OfflineAudioContext(1, inputBuffer.length * OUT_SAMPLE_RATE / inputBuffer.sampleRate, OUT_SAMPLE_RATE);
        const buffer = offlineCtx.createBuffer(1, inputBuffer.getChannelData(0).length, IN_SAMPLE_RATE);
        buffer.copyToChannel(inputBuffer.getChannelData(0), 0);

        const source = offlineCtx.createBufferSource();
        source.buffer = buffer;
        source.connect(offlineCtx.destination);
        source.start(false);

        return offlineCtx.startRendering();
    }

    static _PCMfloat32Toi16(f32Buffer) {
        const i16PCM = new ArrayBuffer(f32Buffer.length * BYTES_PER_SAMPLE);
        const view = new DataView(i16PCM);

        for (let i = 0; i < f32Buffer.length; ++i) {
            const f32 = Math.max(-1, Math.min(1, f32Buffer[i]));
            const i16 = (f32 < 0 ? f32 * 0x8000 : f32 * 0x7FFF);
            view.setInt16(i * BYTES_PER_SAMPLE, i16, true);
        }

        return i16PCM;
    }

    _processAudio(inputBuffer) {
        if (!this._onData) return;	// Callback has not be registered yet

        AudioRecorder._resampleTo16000(inputBuffer)				// To 16-Bit signed PCM
        .then((f32Resampled) => {
			const audioData = f32Resampled.getChannelData(0);	// 1-Channel only
			return AudioRecorder._PCMfloat32Toi16(audioData);	
		})
        .then((arrayBuffer) => this._onData(arrayBuffer))		// Send it to callback
        .catch((err) => {
            console.error(err.message);
            throw err;
        });
    }
}

