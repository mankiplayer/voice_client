/**
 * class Socket
 */
class Socket {

    ////////////////////////////////////////////////////////////////////////////
    // public methods

    constructor(url) {
        this._ws = new WebSocket(url);
        this._ws.binaryType = 'arraybuffer';

        this._ws.onopen = this._onOpen.bind(this);
        this._ws.onclose = this._onClose.bind(this);
        this._ws.onmessage = this._onMessage.bind(this);

        this._onEvent = null;
        this._onStreamData = null;
    }

    set onEvent(callback) {
        if (typeof callback !== 'function') throw new Error('Callback must be a function.');
        this._onEvent = callback;
    }
    set onStreamData(callback) {
        if (typeof callback !== 'function') throw new Error('Callback must be a function.');
        this._onStreamData = callback;
    }

    sendCommand(type, args = []) {
        if (type) {
			if (!Array.isArray(args)) args = [args];
			this._ws.send(JSON.stringify({type, args}));
		}
    }

    sendStream(buffer) {
        this._ws.send(buffer);
    }


    ////////////////////////////////////////////////////////////////////////////
    // protected methods

    _onOpen() {
        console.log('Received socket open event ...');
    }

    _onClose() {
        console.log('Received socket close event ...');
    }

    _onMessage(event) {
		const data = event.data;

        if (data instanceof ArrayBuffer) {
            if (this._onStreamData) this._onStreamData(data);
        } else {
            if (this._onEvent) this._onEvent(data);
        }
    }
}

